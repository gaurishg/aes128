mod aes;


#[cfg(test)]
mod tests {
    pub use crate::aes::singleByteSubstitution;
    use crate::aes::mixSingleColumn;
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn sbox_test_00() {
        assert_eq!(singleByteSubstitution(0 as u8), 0x63);
    }

    #[test]
    fn sbox_test_01()
    {
        assert_eq!(singleByteSubstitution(1 as u8), 0x7c);
    }

    #[test]
    fn sbox_test_9a() {
        assert_eq!(singleByteSubstitution(0x9a as u8), 0xb8);
    }

    #[test]
    fn sbox_test_9f()
    {
        assert_eq!(singleByteSubstitution(0x9f), 0xdb);
    }
    

    #[test]
    fn column_mix_test_1()
    {
        assert_eq!(mixSingleColumn([0xdb, 0x13, 0x53, 0x45]), [0x8e, 0x4d, 0xa1, 0xbc]);
    }
}
